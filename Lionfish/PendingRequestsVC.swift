//
//  PendingRequestsVC.swift
//  Lionfish
//
//  Created by Will Lyons on 4/23/17.
//  Copyright © 2017 Grace Portelance. All rights reserved.
//


import Foundation
import UIKit
import Firebase
import FirebaseDatabase

class PendingRequestsVC : UITableViewController {
    
    //@IBOutlet weak var tableView: UITableView!
    var myOffers: [String] = []
    var yesorno: [Bool] = []
    var ref:FIRDatabaseReference!
    var keepTrak: [String] = []
    var keepTrakEmail: [String] = []
    
    func getPendingRequests(completion: @escaping (NSDictionary)->()){
        let data = ref.child("requests")
        data.observeSingleEvent(of: .value, with: { snapshot in
            if let offerDict = snapshot.value as? NSDictionary {
                completion(offerDict)
            }
        })
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return myOffers.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let myCell = tableView.dequeueReusableCell(withIdentifier: "myRequestCell", for: indexPath)
        myCell.textLabel?.text = myOffers[indexPath.row]
        if yesorno[indexPath.row] == true {
            myCell.backgroundColor = UIColor.green
        }
        else {
            myCell.backgroundColor = UIColor.white
        }
        return myCell
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ref = FIRDatabase.database().reference()
        
        getPendingRequests(completion: )(){ (ans: NSDictionary) in
            self.myOffers.removeAll()
            self.yesorno.removeAll()
            for (email,theOffers) in ans {
                //check which of theOffers our my own
                let defaults = UserDefaults.standard
                let myEmail = defaults.string(forKey: "email")
                if String(describing: email) == myEmail {
                    for (offer, data) in (theOffers as? NSDictionary)! {
                        
                        let dat = data as? NSDictionary
                        let bat = String(describing: dat?.value(forKey:"price"))
                        let str = bat.replacingOccurrences(of: "Optional(", with: "")
                        let str1 = str.replacingOccurrences(of: ")", with: "")
                        self.myOffers.append( String(describing: email) + " " + String(describing: offer) + " Price:" + str1 )
                        self.keepTrakEmail.append(String(describing: email))
                        self.keepTrak.append(String(describing: offer))
                        //determine color of cell based on accepted status
                        let fat = String(describing: dat?.value(forKey: "accepted"))
                        let str2 = fat.replacingOccurrences(of: "Optional(", with: "")
                        let acceptedField = str2.replacingOccurrences(of: ")", with: "")
                        if acceptedField != "unaccepted" {
                            self.yesorno.append(true)
                        }
                        else {
                            self.yesorno.append(false)
                        }
                    }
                }
            }
            self.tableView.reloadData()
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getPendingRequests(completion: )(){ (ans: NSDictionary) in
            self.myOffers.removeAll()
            for (email,theOffers) in ans {
                //check which of theOffers our my own
                let defaults = UserDefaults.standard
                let myEmail = defaults.string(forKey: "email")
                if String(describing: email) == myEmail {
                    for (offer, data) in (theOffers as? NSDictionary)! {
                        
                        let dat = data as? NSDictionary
                        let bat = String(describing: dat?.value(forKey:"price"))
                        let str = bat.replacingOccurrences(of: "Optional(", with: "")
                        let str1 = str.replacingOccurrences(of: ")", with: "")
                        self.myOffers.append( String(describing: email) + " " + String(describing: offer) + " Price:" + str1 )
                        self.keepTrakEmail.append(String(describing: email))
                        self.keepTrak.append(String(describing: offer))
                    }
                }
            }
            self.tableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var requestID = keepTrak[indexPath.row]
        var isAccepted = yesorno[indexPath.row]
        
        guard let detailedVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "pendingRequestDVC") as? PendingRequestDetailedVC else { return }
        let transition = CATransition()
        detailedVC.requestName = requestID
        detailedVC.accepted = isAccepted
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromTop
        self.view.window!.layer.add(transition, forKey: kCATransition)
        self.present(detailedVC, animated: true)
        
        
        
    }
}
