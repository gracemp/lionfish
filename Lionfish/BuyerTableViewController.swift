//
//  BuyerTableViewController.swift
//  Lionfish
//
//  Created by Grace Portelance on 4/22/17.
//  Copyright © 2017 Grace Portelance. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase


class BuyerTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var ref: FIRDatabaseReference!
    @IBOutlet weak var tableView: UITableView!
    var emailOuter = UserDefaults.standard.string(forKey: "email")
    var keepTrack: [String] = []
    var keepTrackEmail: [String] = []

    @IBAction func logout(_ sender: Any) {
        let defaults = UserDefaults.standard
        print(defaults.string(forKey: "email") ?? "hi")
        defaults.removeObject(forKey: "email")
        defaults.removeObject(forKey: "who")
        print(defaults.string(forKey: "email") ?? "hi")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "home")
        self.show(vc!, sender: self)
    }
    @IBAction func settingsButton(_ sender: Any) {
        guard let VC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "settingsTable") as? SettingsTViewController else { return }
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromTop
        view.window!.layer.add(transition, forKey: kCATransition)
        self.present(VC, animated: true)
    }
    @IBAction func backButton(_ sender: Any) {
        
    }
    @IBAction func offerPost(_ sender: Any) {
        let defaults = UserDefaults.standard
        let em = defaults.string(forKey: "email")
        guard let VC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "buyerOffer") as? BuyerOfferViewController else { return }
        let transition = CATransition()
        VC.email = em
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromTop
        view.window!.layer.add(transition, forKey: kCATransition)
        self.present(VC, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        emailOuter = UserDefaults.standard.string(forKey: "email")
        self.ref = FIRDatabase.database().reference()
        tableView.delegate = self
        tableView.dataSource = self
        
        getOffers(completion: )(){ (ans:NSDictionary) in
            self.offers.removeAll()
            if (self.keepTrack.count > 0){
                self.keepTrack.removeAll()
            }
            for (email,theOffers) in ans {
                for (offerNum,_) in (theOffers as? NSDictionary)! {
                    self.offers.append(String(describing: email) + "   " + String(describing: offerNum))
                    self.keepTrackEmail.append(String(describing: email))
                    self.keepTrack.append(String(describing: offerNum))
                }
            }
            self.tableView.reloadData()
        }
       
//        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "customcell")
//        ref.child("offers").observe(.value, with:{ snapshot in
//            let enumerator = snapshot.children
//            while let rest = enumerator.nextObject() as? FIRDataSnapshot {
//                let enu2 = snapshot.children
//                while let fin = enu2.nextObject() as? FIRDataSnapshot{
//                    let info = fin.value as? NSDictionary
//                    //print(info)
//                    self.offers.append(info!)
//                    let enu3 = snapshot.children
//                    while let deeper = enu3.nextObject() as? FIRDataSnapshot{
//                        let mystuff = deeper.value as? NSDictionary
//                        print("MY STUFF BEGIN")
//                        print(mystuff)
//                        print("MY STUFF END")
//                    }
//                }
//                
//            }
//            self.tableView.reloadData()
//        })

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var offers: [String] = ["1","2","3"]
    
    func getOffers(completion: @escaping (NSDictionary)->()){
        let data = ref.child("offers")
        data.observeSingleEvent(of: .value, with: { snapshot in
            if let offerDict = snapshot.value as? NSDictionary {
                completion(offerDict)
            }
            
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offers.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let myCell = UITableViewCell(style: .default, reuseIdentifier: "myCell")
        myCell.textLabel?.text = offers[indexPath.row]
        return myCell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var emailID = keepTrackEmail[indexPath.row]
        var offerName = keepTrack[indexPath.row]
        print(emailID)
        print(offerName)
        var newRef = ref.child("offers").child(emailID).child(offerName)
        
        newRef.observeSingleEvent(of: .value, with: { snapshot in
            
            let value = snapshot.value as? NSDictionary
            print(value)
            let name = value?["name"]!
            let rate = value?["rate"]!
            let amount = value?["amount"]!
            let n1 = String(describing: name!)
            let r1 = String(describing: rate!)
            let a1 = String(describing: amount!)
            guard let detailedVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "offerView") as? OfferViewController else { return }
            let transition = CATransition()
            detailedVC.name1 = n1
            detailedVC.rate1 = r1
            detailedVC.amount1 = a1
            detailedVC.email1 = emailID
            detailedVC.offerName = offerName
            transition.duration = 0.5
            transition.type = kCATransitionPush
            transition.subtype = kCATransitionFromTop
            self.view.window!.layer.add(transition, forKey: kCATransition)
            self.present(detailedVC, animated: true)
        })
        
        
    }


    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


