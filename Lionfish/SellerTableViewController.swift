//
//  SellerTableViewController.swift
//  
//
//  Created by Will Lyons on 4/23/17.
//
//

import UIKit
import Firebase
import FirebaseDatabase

class SellerTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var keepTrack: [String] = []
    var keepTrackEmail: [String] = []
    
    @IBAction func createOffer(_ sender: Any) {
        
        
        let defaults = UserDefaults.standard
        let em = defaults.string(forKey: "email")
        
        guard let detailedVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sellerOffer") as? SellerOfferViewController else { return }
        detailedVC.email = em
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromTop
        self.view.window!.layer.add(transition, forKey: kCATransition)
        self.present(detailedVC, animated: true)

        
    }
    
    
    @IBAction func settingsButton(_ sender: Any) {
        guard let VC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "settingsTable") as? SettingsTViewController else { return }
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromTop
        view.window!.layer.add(transition, forKey: kCATransition)
        self.present(VC, animated: true)
        
    }
    @IBAction func logout(_ sender: Any) {
        let defaults = UserDefaults.standard
        print(defaults.string(forKey: "email") ?? "hi")
        defaults.removeObject(forKey: "email")
        defaults.removeObject(forKey: "who")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "home")
        self.show(vc!, sender: self)
    }
    @IBOutlet var background: UIView!
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true)
    }
    @IBOutlet weak var tableView: UITableView!
    var requests: [String] = []
    var ref:FIRDatabaseReference!
 
    func getRequests(completion: @escaping (NSDictionary)->()){
        let data = ref.child("requests")
        data.observeSingleEvent(of: .value, with: { snapshot in
            if let reqDict = snapshot.value as? NSDictionary {
                completion(reqDict)
            }
        })
    }
    
     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return requests.count
    }
    
    
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let myCell = UITableViewCell(style: .default, reuseIdentifier: "customIdentifier")
        myCell.textLabel?.text = requests[indexPath.row]
        return myCell
     }
    
    @IBAction func offerPost(_ sender: Any) {
        guard let VC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sellerOffer") as? SellerOfferViewController else { return }
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromTop
        view.window!.layer.add(transition, forKey: kCATransition)
        self.present(VC, animated: true)
    }
    override func viewDidLoad() {
        background.backgroundColor = color.lightBlue()
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        self.ref = FIRDatabase.database().reference()
        
        getRequests(completion: )(){ (ans: NSDictionary) in
            self.requests.removeAll()
            for (email,theReqs) in ans {
                for (req, data) in (theReqs as? NSDictionary)! {
                    let dat = data as? NSDictionary
                    let bat = String(describing: dat?.value(forKey:"price"))
                    let str = bat.replacingOccurrences(of: "Optional(", with: "")
                    let str1 = str.replacingOccurrences(of: ")", with: "")
                    self.requests.append( String(describing: email) + String(describing: req) + " Price:" + str1 )
                    self.keepTrackEmail.append(String(describing: email))
                    self.keepTrack.append(String(describing: req))
                }
            }
            self.tableView.reloadData()
        }
        
        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var emailID = keepTrackEmail[indexPath.row]
        var requestName = keepTrack[indexPath.row]
        print(emailID)
              
        
        var newRef = ref.child("requests").child(emailID).child(requestName)
        
        newRef.observeSingleEvent(of: .value, with: { snapshot in
            let value = snapshot.value as? NSDictionary
            
            let name = value?["name"]!
            let price = value?["price"]!
            let amount = value?["amount"]!
            let n1 = String(describing: name!)
            let p1 = String(describing: price!)
            let a1 = String(describing: amount!)
            guard let detailedVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "requestView") as? RequestViewController else { return }
            let transition = CATransition()
            detailedVC.name1 = n1
            detailedVC.price1 = p1
            detailedVC.amount1 = a1
            detailedVC.email1 = emailID
            detailedVC.requestName = requestName
            transition.duration = 0.5
            transition.type = kCATransitionPush
            transition.subtype = kCATransitionFromTop
            self.view.window!.layer.add(transition, forKey: kCATransition)
            self.present(detailedVC, animated: true)
            
            
            
        })

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
}

    

