//
//  Seller.swift
//  Lionfish
//
//  Created by Grace Portelance on 4/10/17.
//  Copyright © 2017 Grace Portelance. All rights reserved.
//

import Foundation

class Seller {
    
    init(username:String, password:String, email:String, address:String, companyName: String, companyRep: String) {
        self.username = username
        self.password = password
        self.email = email
        self.address = address
        self.companyName = companyName
        self.companyRep = companyRep
        numTransactions = 0
        rating = 0
        id = 1
        
    }
    var username : String?
    var password : String?
    var email: String?
    var address: String?
    var companyName: String?
    var companyRep: String?
    var numTransactions: Int
    var rating: Int
    var id: Int
}
