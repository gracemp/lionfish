//
//  ViewController.swift
//  Lionfish
//
//  Created by Grace Portelance on 4/9/17.
//  Copyright © 2017 Grace Portelance. All rights reserved.
//

import UIKit
import Contacts
import MapKit
import CoreLocation
import Firebase
import FirebaseDatabase

class ViewController: UIViewController {

    @IBOutlet var background: UIView!
    @IBOutlet weak var sublabel: UILabel!
    @IBOutlet weak var welcome: UILabel!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var subLabel: UILabel!
    @IBOutlet weak var buyerButton: UIButton!
    @IBOutlet weak var sellerButton: UIButton!
    
    var ref: FIRDatabaseReference!
    let address:String = "6659 Kingsbury Blvd."
    let city:String = "St. Louis"
    let state:String = "MO"
    let zip:String = "63130"
    var coords: CLLocationCoordinate2D?
    
    //getDirections() function...
    @IBAction func infoButton(_ sender: Any) {
        
        let addressString =
        "\(address) \(city) \(state) \(zip)"
        
        CLGeocoder().geocodeAddressString(addressString, completionHandler:
            {(placemarks, error) in
                
                if error != nil {
                    print("Geocode failed: \(error!.localizedDescription)")
                } else if placemarks!.count > 0 {
                    let placemark = placemarks![0]
                    let location = placemark.location
                    self.coords = location!.coordinate
                    self.showMap()
                }
        })
        
    }

    func showMap() {
        let addressDict =
            [CNPostalAddressStreetKey: address,
             CNPostalAddressCityKey: city,
             CNPostalAddressStateKey: state,
             CNPostalAddressPostalCodeKey: zip]
        
        let place = MKPlacemark(coordinate: coords!,
                                addressDictionary: addressDict)
        
        let mapItem = MKMapItem(placemark: place)
        
        let options = [MKLaunchOptionsDirectionsModeKey:
            MKLaunchOptionsDirectionsModeDriving]
        
        mapItem.openInMaps(launchOptions: options)
    }
    
    
  ///////
    
    @IBOutlet weak var signUpCloseView: UIView!
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func loginButton(_ sender: UIButton) {
        guard let loginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginView") as? LoginViewController else { return }
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromTop
        view.window!.layer.add(transition, forKey: kCATransition)
        self.present(loginVC, animated: true)
    }
    
    @IBAction func signUpButton(_ sender: UIButton) {
        signUpCloseView.isHidden = false
        
    }
    
    @IBAction func signUpBuyer(_ sender: UIButton) {
        guard let SignUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewBuyer") as? BuyerSignUpViewController else { return }
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromTop
        view.window!.layer.add(transition, forKey: kCATransition)
        self.present(SignUpVC, animated: true)
    }
    
    @IBAction func signUpSeller(_ sender: UIButton) {
        guard let SignUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewSeller") as? SellerSignUpViewController else { return }
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromTop
        view.window!.layer.add(transition, forKey: kCATransition)
        self.present(SignUpVC, animated: true)
        
    }
    
    
    private let circleButton: [UIButton] = {
        var array = [UIButton]()
        for i in 0 ..< 25 {
        let button = UIButton()
         let size =  CGFloat(arc4random_uniform(50))
        button.frame = CGRect(x: CGFloat((arc4random_uniform(400) + 1)), y: 0, width: size, height: size)
        button.backgroundColor = color.midBlue()
        button.layer.cornerRadius = size/2
        array.append(button)
        }
       return array
    }()

    
    override func viewDidLoad() {
        signUpCloseView.isHidden = true
        self.ref = FIRDatabase.database().reference()
        //color setup
        background.backgroundColor = color.lightBlue()
        loginButton.backgroundColor = color.teal()
        signupButton.backgroundColor = color.teal()
        sublabel.textColor = color.forest()
        welcome.textColor = color.darkBlue()
        buyerButton.backgroundColor = color.teal()
        sellerButton.backgroundColor = color.teal()
        
        loginButton.layer.cornerRadius = 10
        signupButton.layer.cornerRadius = 10
        buyerButton.layer.cornerRadius = 5
        sellerButton.layer.cornerRadius = 5
        super.viewDidLoad()
    }
    override func viewDidAppear(_ animated: Bool) {
        self.ref = FIRDatabase.database().reference()
        signUpCloseView.isHidden = true
        // Do any additional setup after loading the view, typically from a nib.
        for button in circleButton {
        button.center = CGPoint(x:CGFloat((arc4random_uniform(600) + 1)) , y: view.frame.height + 40)
        view.addSubview(button)
        view.sendSubview(toBack: button)
            animateCircle()
        }
        super.viewDidAppear(animated)
        
    }
    
    func animateCircle() {
        for button in self.circleButton {
        let duration = Double(arc4random_uniform(20) + 8)
        UIView.animate(withDuration: duration, delay: 0, options: [.repeat, .curveEaseInOut], animations: {
            button.center.y = -25
            })
        }
}
    

    
}

