//
//  SellerSignUpViewController.swift
//  Lionfish
//
//  Created by Grace Portelance on 4/9/17.
//  Copyright © 2017 Grace Portelance. All rights reserved.
// chage

import UIKit
import Firebase
import FirebaseDatabase

class SellerSignUpViewController: UIViewController {
    var ref: FIRDatabaseReference!
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet var background: UIView!
    @IBOutlet weak var zipField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var invalidLabel: UILabel!
    @IBOutlet weak var addedImage: UIImageView!
    @IBOutlet weak var fullNameField: UITextField!
    @IBOutlet weak var addressField: UITextField!
    @IBOutlet weak var countryField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var cityField: UITextField!
    @IBAction func regisBtn(_ sender: Any) {
        var invalid = false
        let manip = emailField.text!
        let str = manip.replacingOccurrences(of: "@", with: "___")
        let emailIO = str.replacingOccurrences(of: ".", with: "___")
        ref.child(emailIO).setValue("true")
        ref.child("buyers").observe(.value, with:{
            (snapshot) in
            let enumerator = snapshot.children
            while let rest = enumerator.nextObject() as? FIRDataSnapshot {
                let enumerator2 = rest.children
                while let uh = enumerator2.nextObject() as? FIRDataSnapshot {
                    if(emailIO == String(describing: uh.value!)){
                        invalid = true
                    }
                }
            }
            if(invalid){
                self.invalidLabel.text = "Invalid Email"
                self.invalidLabel.isHidden = false
            }
            else{
                let regAddress = self.addressField.text!
                let regCity = self.cityField.text!
                let regCountry = self.countryField.text!
                let zipCode = self.zipField.text!
                let regPhone = self.phoneField.text!
                let regPass = self.passwordField.text!
                let regName = self.fullNameField.text!
                let newData = self.ref.child("sellers").child(emailIO)
                let newBuyer = [
                    "address" : regAddress+" "+regCity+" "+regCountry+" "+zipCode,
                    "password" : regPass ,
                    "email" : emailIO ,
                    "name" : regName ,
                    "phone" : regPhone
                    ] as [String : Any]
                newData.setValue(newBuyer)
                let defaults = UserDefaults.standard
                defaults.removeObject(forKey: "email")
                UserDefaults.standard.set(emailIO, forKey: "email")
                guard let detailedVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sellerTable") as? SellerTableViewController else { return }
                let transition = CATransition()
                transition.duration = 0.5
                transition.type = kCATransitionPush
                transition.subtype = kCATransitionFromTop
                self.view.window!.layer.add(transition, forKey: kCATransition)
                self.present(detailedVC, animated: true)
            }
        }, withCancel: nil)
    }

    @IBAction func backButton(_ sender: Any) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromTop
        view.window!.layer.add(transition, forKey: kCATransition)
        self.dismiss(animated:true)
    }
    
    @IBOutlet weak var registerLabel: UILabel!
    @IBAction func addImage(_ sender: Any) {
        guard let loginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "cameraView") as? CameraViewController else { return }
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromTop
        view.window!.layer.add(transition, forKey: kCATransition)
        self.present(loginVC, animated: true)
    }
    @IBOutlet weak var addImage: UIButton!
    @IBOutlet weak var smallView: UIView!
    
    override func viewDidLoad() {
        self.ref = FIRDatabase.database().reference()
        background.backgroundColor = color.lightBlue()
        smallView.backgroundColor = color.midBlue()
        registerLabel.textColor = color.darkBlue()
        addImage.backgroundColor = color.greyBlue()
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
