//
//  OfferViewController.swift
//  Lionfish
//
//  Created by Charles Manzella on 4/23/17.
//  Copyright © 2017 Grace Portelance. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase
//
class OfferViewController: UIViewController{
    var ref: FIRDatabaseReference!
    var address1: String!
    var email1: String!
    var name1: String!
    var phone1: String!
    var rate1: String!
    var time: String!
    var amount1: String!
    var offerName: String!

    @IBOutlet weak var rate: UILabel!
    @IBOutlet weak var amount: UILabel!
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var email: UILabel!
    
    override func viewDidLoad(){
        rate.text = rate1
        email.text = email1
        name.text = name1
        amount.text = amount1
        self.ref = FIRDatabase.database().reference()
        super.viewDidLoad()
    }
    override func viewDidAppear(_ animated: Bool) {
        self.ref = FIRDatabase.database().reference()
    }

    @IBAction func back(_ sender: Any) {
        
        guard let detailedVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "buyerTab") as?UITabBarController else { return }
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromTop
        self.view.window!.layer.add(transition, forKey: kCATransition)
        self.present(detailedVC, animated: true)
        
        
        
    }
    
    @IBAction func acceptTheOffer(_ sender: Any) {
    
    
        let defaults = UserDefaults.standard
        let buyer_email = defaults.string(forKey: "email")
        print(buyer_email)
          //get users email from user defaults
    //    var buyer_email = "buyeremail___gmail___com"
        let newdata = ref.child("offers").child(email1).child(offerName)
        
        newdata.observeSingleEvent(of: .value, with: { snapshot in
            let value = snapshot.value as? NSDictionary
            let a = value?["accepted"]!
            let accepted = String(describing: a!)
            print(accepted)
            if(accepted == "unaccepted"){
                newdata.child("accepted").setValue(buyer_email)
            } else {
                let alertController = UIAlertController(title: "iOScreator", message:
                    "This offer has been accepted", preferredStyle: UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default,handler: nil))
                
                self.present(alertController, animated: true, completion: nil)

            }
            
        
        })
    }
    
    
    
    
    
}
