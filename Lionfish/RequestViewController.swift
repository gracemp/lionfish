//
//  RequestViewController.swift
//  Lionfish
//
//  Created by Charles Manzella on 4/22/17.
//  Copyright © 2017 Grace Portelance. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase
//
class RequestViewController: UIViewController{
    var ref: FIRDatabaseReference!
    var address1: String!
    var email1: String!
    var name1: String!
    var phone1: String!
    var price1: String!
    var time: String!
    var amount1: String!
    var requestName: String!
    
    @IBAction func back(_ sender: Any) {
        
        guard let detailedVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sellerTab") as?UITabBarController else { return }
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromTop
        self.view.window!.layer.add(transition, forKey: kCATransition)
        self.present(detailedVC, animated: true)
        

        
        
    }
    
    
    
    @IBOutlet weak var amt: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var name: UILabel!
 
    override func viewDidLoad(){
        price.text = price1
        email.text = email1
        name.text = name1
        amt.text = amount1
        self.ref = FIRDatabase.database().reference()
        super.viewDidLoad()
    }
    override func viewDidAppear(_ animated: Bool) {
        self.ref = FIRDatabase.database().reference()

    }
    
    @IBAction func respondRequest(_ sender: Any) {
        let defaults = UserDefaults.standard
        
    var seller_email = defaults.string(forKey: "email")
   
    
          //get from user defaults
   //     var seller_email = "selleremail___gmail___com"
        
        
        let newdata = ref.child("requests").child(email1).child(requestName)
        
        newdata.observeSingleEvent(of: .value, with: { snapshot in
            let value = snapshot.value as? NSDictionary
            let a = value?["accepted"]!
            let accepted = String(describing: a!)
            print(accepted)
            if(accepted == "unaccepted"){
                newdata.child("accepted").setValue(seller_email)
            } else {
                let alertController = UIAlertController(title: "iOScreator", message:
                    "This request has been accepted", preferredStyle: UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default,handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
                
            }

            
            
            
            
        

        
        })
   
    }
}
