//
//  ColorExtension.swift
//  Lionfish
//
//  Created by Grace Portelance on 4/18/17.
//  Copyright © 2017 Grace Portelance. All rights reserved.
//

import UIKit
import Foundation
class color: NSObject{
    
    
    class func darkBlue () -> UIColor {
        return UIColor(red: 24/255, green:35/255, blue: 53/255, alpha: 1.0)
}
    class func midBlue () -> UIColor {
       return UIColor(red: 137/255, green:201/255, blue: 210/255, alpha: 1.0)
    }
    
    class func greyBlue () -> UIColor {
        return UIColor(red: 98/255, green:145/255, blue: 155/255, alpha: 1.0)
    }
    class func lightBlue () -> UIColor {
        return UIColor(red: 218/255, green:232/255, blue: 244/255, alpha: 1.0)
    }
    class func forest () -> UIColor {
        return UIColor(red: 54/255, green:95/255, blue: 91/255, alpha: 1.0)
    }
    class func teal () -> UIColor {
        return UIColor(red: 55/255, green:112/255, blue: 123/255, alpha: 1.0)
    }
    
    
    
}

//    }
//    var lightSand = UIColor(red: 240/255, green:236/255, blue: 235/255, alpha: 1.0)
//    let darkSand = UIColor(red: 240/255, green:228/255, blue: 221/255, alpha: 1.0)
//    let lightBlue = UIColor(red: 136/255, green:163/255, blue: 173/255, alpha: 1.0)
//    let darkBlue = UIColor(red: 119/255, green:135/255, blue: 143/255, alpha: 1.0)
//    let lightGray = UIColor(red: 170/255, green:185/255, blue: 189/255, alpha: 1.0)
//    let darkGray = UIColor(red: 119/255, green:135/255, blue: 189/255, alpha: 1.0)
//}
