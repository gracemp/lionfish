//
//  PendingOffersVC.swift
//  
//
//  Created by Will Lyons on 4/23/17.
//
//


import Foundation
import UIKit
import Firebase
import FirebaseDatabase

class PendingOffersVC : UITableViewController {
    
    //@IBOutlet weak var tableView: UITableView!
    var myOffers: [String] = []
    var yesorno: [Bool] = []
    var ref:FIRDatabaseReference!
    
    func getPendingOffers(completion: @escaping (NSDictionary)->()){
        let data = ref.child("offers")
        data.observeSingleEvent(of: .value, with: { snapshot in
            if let offerDict = snapshot.value as? NSDictionary {
                completion(offerDict)
            }
        })
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return myOffers.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let myCell = tableView.dequeueReusableCell(withIdentifier: "myOfferCell", for: indexPath)
        myCell.textLabel?.text = myOffers[indexPath.row]
        if yesorno[indexPath.row] == true {
            myCell.backgroundColor = UIColor.green
        }
        else {
            myCell.backgroundColor = UIColor.white
        }
        return myCell
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //tableView.dataSource = self
        //tableView.delegate = self
        self.ref = FIRDatabase.database().reference()
        
        getPendingOffers(completion: )(){ (ans: NSDictionary) in
            self.myOffers.removeAll()
            self.yesorno.removeAll()
            for (email,theOffers) in ans {
                //check which of theOffers our my own
                let defaults = UserDefaults.standard
                let myEmail = defaults.string(forKey: "email")
                if String(describing: email) == myEmail {
                    for (offer, data) in (theOffers as? NSDictionary)! {
                        
                        let dat = data as? NSDictionary
                        let bat = String(describing: dat?.value(forKey:"rate"))
                        let str = bat.replacingOccurrences(of: "Optional(", with: "")
                        let str1 = str.replacingOccurrences(of: ")", with: "")
                        self.myOffers.append( String(describing: email) + " " + String(describing: offer) + " Rate:" + str1 )
                        //determine color of cell based on accepted status
                        let fat = String(describing: dat?.value(forKey: "accepted"))
                        let str2 = fat.replacingOccurrences(of: "Optional(", with: "")
                        let acceptedField = str2.replacingOccurrences(of: ")", with: "")
                        if acceptedField != "unaccepted" {
                            self.yesorno.append(true)
                        }
                        else {
                            self.yesorno.append(false)
                        }
                    }
                }
            }
            self.tableView.reloadData()
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getPendingOffers(completion: )(){ (ans: NSDictionary) in
            self.myOffers.removeAll()
            for (email,theOffers) in ans {
                //check which of theOffers our my own
                let defaults = UserDefaults.standard
                let myEmail = defaults.string(forKey: "email")
                if String(describing: email) == myEmail {
                    for (offer, data) in (theOffers as? NSDictionary)! {
                        
                        let dat = data as? NSDictionary
                        let bat = String(describing: dat?.value(forKey:"rate"))
                        let str = bat.replacingOccurrences(of: "Optional(", with: "")
                        let str1 = str.replacingOccurrences(of: ")", with: "")
                        self.myOffers.append( String(describing: email) + " " + String(describing: offer) + " Rate:" + str1 )
                    }
                }
            }
            self.tableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
