//
//  PendingRequestDetailedVC.swift
//  Lionfish
//
//  Created by Will Lyons on 4/23/17.
//  Copyright © 2017 Grace Portelance. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseDatabase

class PendingRequestDetailedVC: UIViewController{
    var ref: FIRDatabaseReference!
    var requestName: String!
    var accepted: Bool!
    
    @IBAction func back(_ sender: Any) {
           self.dismiss(animated:true)
        
    
    }
    @IBOutlet weak var aMarker: UILabel!
    @IBOutlet weak var pMarker: UILabel!
    @IBOutlet weak var eMarker: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var theTItle: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var phone: UILabel!
    
    @IBAction func map(_ sender: Any) {
    }
    override func viewDidLoad() {
        self.ref = FIRDatabase.database().reference()
        aMarker.isHidden = true
        pMarker.isHidden = true
        eMarker.isHidden = true
        emailLabel.isHidden = true
        addressLabel.isHidden = true
        phone.isHidden = true
        theTItle.text = requestName
        let defaults = UserDefaults.standard
        let email = defaults.string(forKey: "email")
        let emailUnwrap = email!
        print(emailUnwrap)
        print(requestName)
        let newdata = ref.child("requests").child(emailUnwrap).child(requestName)
        
        newdata.observeSingleEvent(of: .value, with: { snapshot in
            let value = snapshot.value as? NSDictionary
            print(value)
            let aE = value?["accepted"]!
            print(aE)
            let pr = value?["price"]
            let am = value?["amount"]
            let price = String(describing: pr)
            let amount = String(describing: am)
            let accepterEmail = String(describing: aE!)
            
            print(accepterEmail)
            self.priceLabel.text = price
            self.amountLabel.text = amount
            
            if(accepterEmail != "unaccepted"){
                self.ref.child("sellers").child(accepterEmail).observe(.value, with: {snapshot in
                    self.status.text = "accepted"
                    self.emailLabel.text = accepterEmail
                        let value = snapshot.value as? NSDictionary
                        let a = value?["address"]
                        let address = String(describing: a!)
                        let p = value?["phone"]
                    let phone = String(describing: p!)
                  
                    self.phone.text = phone
                    self.addressLabel.text = address
                    self.aMarker.isHidden = false
                    self.pMarker.isHidden = false
                    self.eMarker.isHidden = false
                    self.emailLabel.isHidden = false
                    self.addressLabel.isHidden = false
                    self.phone.isHidden = false
                })
            }
            else{
                self.aMarker.isHidden = true
                self.pMarker.isHidden = true
                self.eMarker.isHidden = true
                self.emailLabel.isHidden = true
                self.addressLabel.isHidden = true
                self.phone.isHidden = true
                self.status.text = "unaccepted"
                
            }
        })
    }
    override func viewDidAppear(_ animated: Bool) {
        self.ref.database.reference()

    }
    
}
