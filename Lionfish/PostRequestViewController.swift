//
//  PostRequestViewController.swift
//  Lionfish
//
//  Created by Charles Manzella on 4/23/17.
//  Copyright © 2017 Grace Portelance. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseDatabase

class PostRequestViewController: UIViewController{
    var ref: FIRDatabaseReference!
    var email: String!
    var r1: String!
    var amount: String!
    
    
    @IBOutlet weak var quantity: UITextField!
    
    
    @IBOutlet weak var price: UITextField!
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.ref = FIRDatabase.database().reference()
    }
    override func viewDidLoad() {
        self.ref = FIRDatabase.database().reference()
    }
    
    @IBAction func uploadRequest(_ sender: Any) {
    
    if (price.text == "" || quantity.text == ""){
            
            let alertController = UIAlertController(title: "iOScreator", message:
                "Please fill out the rate and quantity fields", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            
            //get name based off email
            let theData = ref.child("buyers").child(email)
            var name = ""
            theData.observeSingleEvent(of: .value, with: { snapshot in
                let sellerInfo = snapshot.value as? NSDictionary
                let n1 = sellerInfo?["name"]!
                name = String(describing: n1!)
                print(name)
            })
            let newData = ref.child("requests").child(email)
            newData.observeSingleEvent(of: .value, with: { snapshot in
                let value = snapshot.value as? NSDictionary
                let count1 = value?.count
                print(count1)
                let counter = String(count1!+1)
                print(counter)
                let temp1 = "request"+counter
                print(temp1)
                let dict1: [String: String] = ["price": self.price.text!, "amount": self.quantity.text!, "name": name, "accepted": "unaccepted"]
                
                newData.child(temp1).setValue(dict1)
//                let dict2: [String: String] = ["response1":"noone"]
//                newData.child(temp1).child("responses").setValue(dict2)
                
            })
        }
    }
}
