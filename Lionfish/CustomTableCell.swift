//
//  CustomTableCell.swift
//  Lionfish
//
//  Created by Grace Portelance on 4/10/17.
//  Copyright © 2017 Grace Portelance. All rights reserved.
//

import Foundation

import UIKit

class CustomTableViewCell: UITableViewCell {
    
    @IBOutlet weak var buyerSpecs: UILabel!
    @IBOutlet weak var buyerImage: UIImageView!
    @IBOutlet weak var buyerName: UILabel!
    
    @IBAction func goToOffer(_ sender: Any) {
    }
}
