//
//  LoginViewController.swift
//  Lionfish
//
//  Created by Grace Portelance on 4/9/17.
//  Copyright © 2017 Grace Portelance. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class LoginViewController: UIViewController {
    var ref: FIRDatabaseReference!
    @IBOutlet weak var usernameField: UITextField!
     @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var invalidField: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet var background: UIView!
    override func viewDidLoad() {
        invalidField.text = ""
        self.ref = FIRDatabase.database().reference()
        background.backgroundColor = color.lightBlue()
        loginLabel.textColor = color.darkBlue()
        loginButton.backgroundColor = color.teal()
        loginButton.layer.cornerRadius = 5;
        if (passwordField.isEditing){
        passwordField.isSecureTextEntry = true
        }
        passwordField.layer.cornerRadius = 5
        usernameField.layer.cornerRadius = 5
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButton(_ sender: Any) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromTop
        view.window!.layer.add(transition, forKey: kCATransition)
        self.dismiss(animated: true)
    }
    @IBAction func loginButton(_ sender: Any) {
        var invalid = "XXXX"
        let manip = usernameField.text!
        let str = manip.replacingOccurrences(of: "@", with: "___")
        let emailIO = str.replacingOccurrences(of: ".", with: "___")
         ref.child(emailIO).observe(.value, with:{
            (snapshot) in
            invalid = snapshot.value as! String
            var who = "failure"
            if(invalid == "false"){
                who = "buyers"
            }
            else if(invalid == "true"){
                who = "sellers"
            }
            else{
               who = "nope"
            }
            if(who != "nope"){
                self.ref.child(who).child(emailIO).observe(.value, with: {
                    (snapshot) in
                    let dict = snapshot.value as? NSDictionary
                    let pass = dict?["password"]!
                    let t1 = self.passwordField.text!.replacingOccurrences(of: " ", with: "")
                    let t2 = String(describing: pass!).replacingOccurrences(of: " ", with: "")
                    if(t1 == t2){
                        let defaults = UserDefaults.standard
                        defaults.removeObject(forKey: "email")
                        UserDefaults.standard.set(emailIO, forKey: "email")
                        if(who == "buyers"){
                            guard let detailedVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "buyerTab") as?UITabBarController else { return }
                            let transition = CATransition()
                            transition.duration = 0.5
                            transition.type = kCATransitionPush
                            transition.subtype = kCATransitionFromTop
                            self.view.window!.layer.add(transition, forKey: kCATransition)
                            self.present(detailedVC, animated: true)
                            
                            
                        } else{
                            
                            guard let detailedVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sellerTab") as? UITabBarController else { return }
                            let transition = CATransition()
                            transition.duration = 0.5
                            transition.type = kCATransitionPush
                            transition.subtype = kCATransitionFromTop
                            self.view.window!.layer.add(transition, forKey: kCATransition)
                            self.present(detailedVC, animated: true)
                        }
                        print("SUCCESSS")
                    }
                })
            }
        })
        
    }
}
