//
//  SellerOfferViewController.swift
//  Lionfish
//
//  Created by Grace Portelance on 4/22/17.
//  Copyright © 2017 Grace Portelance. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
class SellerOfferViewController: UIViewController {
    var email: String!
    var ref: FIRDatabaseReference!
    @IBOutlet var background: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var quantityField: UITextField!
    @IBOutlet weak var descriptionText: UITextView!

    @IBOutlet weak var postButton: UIButton!
    @IBOutlet weak var priceField: UITextField!
    
    @IBAction func postButton(_ sender: Any) {
        if (priceField.text == "" || quantityField.text == ""){
            
            let alertController = UIAlertController(title: "iOScreator", message:
                "Please fill out the rate and quantity fields", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            
            //get name based off email
            let theData = ref.child("sellers").child(email)
            var name = ""
            theData.observeSingleEvent(of: .value, with: { snapshot in
                let sellerInfo = snapshot.value as? NSDictionary
                let n1 = sellerInfo?["name"]!
                name = String(describing: n1!)
                print(name)
            })
            let newData = ref.child("offers").child(email)
            newData.observeSingleEvent(of: .value, with: { snapshot in
                let value = snapshot.value as? NSDictionary
                let count1 = value?.count
                print(count1)
                let counter = String(count1!+1)
                print(counter)
                let temp1 = "offer"+counter
                print(temp1)
                let dict1: [String: String] = ["rate": self.priceField.text!, "amount": self.quantityField.text!, "name": name, "accepted": "unaccepted"]
                
                newData.child(temp1).setValue(dict1)
                
                
            })
        }
    }


        
        
        
    
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true)
    }

    override func viewDidLoad() {
        background.backgroundColor = color.lightBlue()
        titleLabel.textColor = color.darkBlue()
        postButton.backgroundColor = color.teal()
        self.ref = FIRDatabase.database().reference()
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
