//
//  Buyer.swift
//  Lionfish
//
//  Created by Grace Portelance on 4/10/17.
//  Copyright © 2017 Grace Portelance. All rights reserved.
//

import Foundation

class Buyer {
    
    init(username:String, password:String, email:String, address:String, fullName: String) {
        self.username = username
        self.password = password
        self.email = email
        self.address = address
        self.fullName = fullName
        numTransactions = 0
        rating = 0
        id = 0
        image = ""
       
        
    }
    var username : String?
    var password : String?
    var email: String?
    var address: String?
    var fullName: String?
    var numTransactions: Int
    var rating: Int
    var id: Int
    var image : String
}
